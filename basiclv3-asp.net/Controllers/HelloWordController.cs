﻿using Microsoft.AspNetCore.Mvc;

namespace basiclv3_asp.net.Controllers
{
    public class HelloWordController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Welcome(string name, int numTimes = 1)
        {
            ViewData["Message"] = "Hello " + name;
            ViewData["NumTimes"] = numTimes;

            return View();
        }
    }
}
