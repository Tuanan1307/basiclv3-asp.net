﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using basiclv3_asp.net.Models;

namespace basiclv3_asp.net.Data
{
    public class basiclv3_aspnetContext : DbContext
    {
        public basiclv3_aspnetContext (DbContextOptions<basiclv3_aspnetContext> options)
            : base(options)
        {
        }

        public DbSet<basiclv3_asp.net.Models.Movie> Movie { get; set; } = default!;
    }
}
